# Picker
##Project builder for HTML, CSS and JavaScript based on Gulp

![alt text](app/images/bg.jpg)

Author: [Vitaliy Shevchuk](https://www.youtube.com/channel/UCQ2Cy1p6PxeJDHOW4nPij5A)

Picker is project builder for HTML, CSS and Javascript based on Gulp. Picker uses **bower** for include libraries like jQuery and FontAwesome icons version 4 (They are already included in the Picker). Gulp uses modules like sass, browser-sync, autoprefixer, clean-css, imagemin, rsync, rigger and cache.

**Picker** is perfect for beginner and experienced frontend developers. Your completed project is in the "builder" folder.

Picker uses **Sass** syntax, but if you don't now Sass you can only use **CSS**.
Cross-browser: IE9+.

## How to use Picker

1. [Download](https://bitbucket.org/vita1ya/picker/get/3de838b6660e.zip) Picker from BitBucket;
2. Install Node Modules(in terminal): **npm i**;
3. Run the builder(in terminal): **gulp**.

## What Picker can do

1. Split HTML code into components and include them in the main HTML files. The attached files are in the "components" folder. Example:  
**index.html**  
`//= components/menu.html`  
This code includes your HTML file "menu.html" for "index.html".
2. Sass or CSS styles minify and clean to build/css/main.min.css. For include libraries to the project use "sass/_libs.sass" file. For include fonts us "sass/_fonts.sass" file.
3. JavaScript code concat and minify to build/js/scripts.min.js.
4. For install new libraries run the command in terminal:  
`bower i plugin-name`  
Bower must be installed in the system (*npm i -g bower*). Libraries are automatically placed in folder "app/libs". For include library to the project you need to write the path to gulpfile.js in gulp.task('scripts:build').
5. For include other files to the project use gulpfile.js gulp.task('other:build').